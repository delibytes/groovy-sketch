//
// Generated from archetype; please customize.
//

package org.example

import org.junit.Test
/**
 * Tests for the {@link Helper} class.
 */
class HelperTest
{
    @Test
    void testHelp() {
        new Helper().help(new Example())
    }
}
